package br.com.arbitration.run;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class ArbitrationApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(ArbitrationApiApplication.class, args);
	}
}
